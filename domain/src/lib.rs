#![feature(drop_types_in_const)]
#![feature(asm)]
#![feature(arc_counts)]
#![feature(cfg_target_thread_local)] 
#![feature(const_fn)]

// TODO: remove unwraps where unsafe
// TODO: check lock scope and deadlocks

//Added for Semaphore implementation 
extern crate sema;

#[macro_use]
mod tls;

pub mod domain; 
pub use domain::*;
