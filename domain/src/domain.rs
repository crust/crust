    use sema::*;
    use super::tls::*;
    use std::sync::{Arc, Weak, Mutex};
    use std::thread;
    use std::panic;
    use std::ops::Deref;
    use std::collections::HashMap;
    use std::cell::UnsafeCell;

    #[inline]
    unsafe fn rdtscp() -> u64 {
        let mut low: u32;
        let mut high: u32;
        asm!("rdtscp" : "={eax}" (low), "={edx}" (high) ::: "volatile");
            ((high as u64) << 32) | (low as u64)
    }

    // Per-thread pointer to the current protection domain the thread
    // is running in
    tls_scoped!(static TLS_CURRENT_DOMAIN: Arc<Mutex<Domain>>);

    pub enum RError {
        Panic,
        Status(i32)
    }

    pub type RRet<T> = Result<T, RError>;

    pub struct Domain {
        // remote references pointing to this domain
        rrefs: HashMap<usize, Box<Sync+Send>>,
        dead: bool,
        destroy_sem: Arc<Semaphore>,
    }

    impl Drop for Domain {
        fn drop(&mut self) {
            self.destroy_sem.post();
            //println!("TLS-trace");
            //unsafe {
             //   for v in trace.iter() {
              //      println!("TLS_CYC {}",v)
              //  }
            //}
        }
    }

    impl Domain {
        // Create new domain
        // f - domain initialization function, run in the current thread
        pub fn new() -> Arc<Mutex<Domain>> {
                Arc::new(Mutex::new(Domain{ rrefs:       HashMap::new()
                                          , dead:        false
                                          , destroy_sem: Arc::new(Semaphore::new(0))}))
        }

        pub fn execute<T, F> (d: &Arc<Mutex<Domain>>, f: F) -> RRet<T>
            where F: FnOnce()->RRet<T>
        {
            let ret = TLS_CURRENT_DOMAIN.set(d, move || 
                                         panic::catch_unwind(panic::AssertUnwindSafe(f))); 
            //let ret = TLS_CURRENT_DOMAIN.set(d, f);
            //println!("retuurning from exec");
            //ret
            match ret {
                Ok(r)  => r, /* TODO: likely */
                Err(_) => {
                    //Domain::destroy(d);
                    println!("[Err] Domain panicked!");
                    Err(RError::Panic)
                }
            }
        }

        #[inline]
        pub fn execute_fastpath<T, F> (f: F) -> RRet<T>
            where F: FnOnce()->RRet<T>
        {
             
            let ret = panic::catch_unwind(panic::AssertUnwindSafe(f)); 
            //let ret = f(); 
            //let ret = TLS_CURRENT_DOMAIN.set(d, f);
            //println!("retuurning from exec");
            //ret
            match ret {
                Ok(r)  => r, /* TODO: likely */
                Err(_) => {
                    //Domain::destroy(d);
                    println!("[Err] Domain panicked!");
                    Err(RError::Panic)
                }
            }
        }

        //Test if we are running within domain, needed for special case
        pub fn in_domain() -> bool {
            TLS_CURRENT_DOMAIN.is_set()
        }

        //Get a reference to domain from TLS
        pub fn get_domain() -> Weak<Mutex<Domain>> {
            let domain = TLS_CURRENT_DOMAIN.with(|d|d.clone());
            Arc::downgrade(&domain)
        }

        // destroy the domain we're running in
        fn exit() {
            println!("calling domain exit!");
            TLS_CURRENT_DOMAIN.with(|d|Domain::destroy(d));
            panic!();
        }

        // TODO: override this function to enable recovery
        // TODO: poison mutexes and anything else the domain may be waiting on
        // Destroy domain (possibly, current domain).  
        // Consumes reference to domain and returns a semaphore to wait on for the domain
        // to be dead.
        fn destroy(d: &Arc<Mutex<Domain>>) {
            // mark domain for destruction (so no more references can get created to it)
            println!("calling domain destroy");
            d.lock().unwrap().dead = true;
            // Release references.  This will call destructors on references; 
            // hence must execute inside the domain
            //Domain::execute(d, ||TLS_CURRENT_DOMAIN.with(|dom|{dom.lock().unwrap().rrefs.clear(); Ok(())}));
            Domain::execute(d, ||{d.lock().unwrap().rrefs.clear(); Ok(())});
        }

        // get semaphore to wait on for domain's death
        pub fn destroy_sem(d: &Arc<Mutex<Domain>>) -> Arc<Semaphore> {
            d.lock().unwrap().destroy_sem.clone()
        }

        fn register_rref<T:Sync+Send+'static>(&mut self, r: &Arc<T>) {
            if self.dead {
                panic!();
            };
            let v = Box::new(r.clone()) as Box<Sync+Send>;
            let k: usize = r.deref() as *const T as usize;
            self.rrefs.insert(k,v);
        }

        fn unregister_rref<T:Sync+Send+'static>(&mut self, r: &Arc<T>) {
            let k: usize = r.deref() as *const T as usize;
            self.rrefs.remove(&k);
        }
    }

    // The internals of RRef are referenced from exactly two places:
    // 1. a weak pointer from RRef
    // 2. a strong pointer from the domain's pointer table
    // Only the former is used to call methods on the reference.
    // The latter is only used to invoke the destructor when the domain
    // is being destroyed.  In both cases the reference can only have one
    // accessor at a time.  It is therefore non necessary to protect it with 
    // a mutex.  We use UnsafeCell instead.
    struct RRefInternal<T> {
        x: UnsafeCell<T>
    }
    
    unsafe impl<T:Send> Sync for RRefInternal<T>{}
    impl<T> RRefInternal<T> {
        fn get(&self) -> &mut T {
            unsafe{&mut *self.x.get()}
        }
    }
    
    impl<T> RRefInternal<T> {
        fn unwrap(self) -> T {
            unsafe{self.x.into_inner()}
        }
    }
    pub struct RRef<T:Sized+Send+'static> {
        i: Weak<RRefInternal<T>>, 
        domain: Weak<Mutex<Domain>>,
        trace : Vec<u64>
    }

    impl<T:Sized+Send+'static> Drop for RRef<T> {
        fn drop(&mut self) {
            //println!("drop for rref called");
            if let Some(d) = self.domain.upgrade() {
                if let Some(r) = self.i.upgrade() {
                    d.lock().unwrap().unregister_rref(&r);
                }
            }
            //println!("TLS-trace");
            //unsafe {
            //   for v in &self.trace {
             //       println!("TLS_CYC {}",v)
             //   }
            //}
        }
    }

    impl<T:Sized+Send+'static> RRef<T> {
        pub fn new(x: T) -> RRef<T> {
            /* To get rid of TLS, add domain reference to this
             * function, do the clone outside this function */
            //let r = Arc::new(RRefInternal{x:UnsafeCell::new(x)});
            let r = Arc::new(RRefInternal{x:UnsafeCell::new(x)});
            let domain = TLS_CURRENT_DOMAIN.with(|d|d.clone());
            // register new interface with domain
            domain.lock().unwrap().register_rref(&r);
            RRef{ i:      Arc::downgrade(&r)
                , domain: Arc::downgrade(&domain)
                , trace: Vec::new()
                }
        }
    }

    impl<T:Sized+Send+'static> RRef<T> {
    #[inline]
        pub fn call<F,R>(&mut self, f: F, trace_type : u32, recovery_on : bool) -> RRet<R>
            where F:FnOnce(&mut T) -> RRet<R> {
            let ret;
            let mut a : u64 = 0;
            let mut b : u64 = 0;
           
            match self.i.upgrade() {
                /* TODO: likely */
                Some(r) => { 
                    match self.domain.upgrade() {
                        /* TODO: likely */
                        Some(d) => {
                            ret = Domain::execute(&d,move||f(r.get()));
                            match ret {
                                Ok(_) => {},
                                Err(_) => {
                                    if !recovery_on {
                                        Domain::destroy(&d);
                                    }
                                },
                            }
                            ret
                        }
                         None    => Err(RError::Panic),
                    }
                },
                None    => Err(RError::Panic)
            }
        /*

            match self.i.upgrade() {
                /* TODO: likely */
                Some(r) => { 
                            ret = Domain::execute_fastpath(move||f(r.get()));
                            //ret = f(r.get());
                            match ret {
                                Ok(_) => {},
                                Err(_) => {
                                    if !recovery_on {
                                        let y = self.domain.upgrade();
                                        match y {
                                            Some(d) => {
                                                Domain::destroy(&d);
                                            },
                                            None => {},
                                        }
                                    }
                                },
                            }
                            ret
                },
                None    => Err(RError::Panic)
            }  
            */
        }
    }

   impl<T:Sized+Send+'static> RRef<T> {
        pub fn call_immut<F,R>(&self, f: F, trace_type : u32, recovery_on : bool) -> RRet<R>
            where F:FnOnce(&mut T) -> RRet<R> {
            let ret;
            //println!("pd call");
            match self.i.upgrade() {
                /* TODO: likely */
                Some(r) => { 
                    match self.domain.upgrade() {
                        /* TODO: likely */
                        Some(d) => {
                            ret = Domain::execute(&d,move||f(r.get()));
                            match ret {
                                Ok(_) => {},
                                Err(_) => {
                                    if !recovery_on {
                                        Domain::destroy(&d);
                                    }
                                },
                            }
                            ret
                        }
                         None    => Err(RError::Panic),
                    }
                },
                None    => Err(RError::Panic)
            }
        }
    }

    impl<T:Sized+Send+'static> RRef<T> {
        pub fn call_owned<F,R>(&self, f: F, trace_type : u32, recovery_on: bool) -> RRet<R>
            where F:FnOnce(T) -> RRet<R> {
            let ret;
            match self.i.upgrade() {
                /* TODO: likely */
                Some(r) => {
                    match self.domain.upgrade() {
                        /* TODO: likely */
                        Some(d) => {
                            d.lock().unwrap().unregister_rref(&r);
                            match Arc::try_unwrap(r) {
                                Ok(v) => {
                                    ret = Domain::execute(&d,move||f(v.unwrap()));
                                    match ret {
                                        Ok(_) => {},
                                        Err(_) => {
                                            if !recovery_on {
                                                Domain::destroy(&d);
                                            }
                                        },
                                    }
                                    ret
                                }
                               Err(_) => panic!("cannot obtain ownership to inner value"),
                            }    
                        }
                        None    => Err(RError::Panic)
                    }
                },
                None    => Err(RError::Panic)
            }
        }
    }

    pub fn spawn<F,T>(f: F) -> thread::JoinHandle<T> 
        where F: FnOnce() -> T, F:Send+'static, T: Send+'static
    {
        let domain = TLS_CURRENT_DOMAIN.with(|d|d.clone());
        thread::spawn(move||TLS_CURRENT_DOMAIN.set(&domain, f))
    }
   
#[cfg(test)]
mod tests {
    use domain::*;
    use std::sync::{Arc};
    use sema::*;

    pub trait IFace1 {
        fn f1(&mut self) -> RRet<u32>;
    }

    pub trait IFace2 {
        fn f2(&mut self) -> RRet<u32>;
    }

    pub struct Struct1 {
        pub iface2: RRef<Struct2>
    }

    impl IFace1 for Struct1 {
        fn f1(&mut self) -> RRet<u32> {
            self.iface2.f2()
        }
    }

    pub struct Struct2 {
        pub x : u32
    }

    impl IFace2 for Struct2 {
        fn f2(&mut self) -> RRet<u32> {
            panic!("crash!")
            //Ok(self.x)
        }
    }

    impl<T:IFace1+Send> IFace1 for RRef<T> {
        fn f1(&mut self) -> RRet<u32> {
            self.call(|x|x.f1())
        }
    }

    impl<T:IFace2+Send> IFace2 for RRef<T> {
        fn f2(&mut self) -> RRet<u32> {
            self.call(|x|x.f2())
        }
    }

    #[test]
    fn it_works() {
        let s2: Arc<Semaphore>;
        let s1: Arc<Semaphore>;
        {
            let d2 = Domain::new();
            let d1 = Domain::new();
            s2 = Domain::destroy_sem(&d2);
            s1 = Domain::destroy_sem(&d1);

            let mr2:RRet<RRef<_>> = Domain::execute(&d2, ||Ok(RRef::new(Struct2{x: 1})));
            let r2 = match mr2 {
                Ok(r2) => r2,
                Err(e) => panic!("Failed to create domain 2")
            };
            let mr1:RRet<RRef<_>> = Domain::execute(&d1, move||Ok(RRef::new(Struct1{iface2: r2})));
            let mut r1 = match mr1 {
                Ok(r1) => r1,
                Err(_) => panic!("Failed to create domain 1")
            };

            match r1.f1() {
                Ok(ret) => println!("Result: {}", ret),
                Err(_)  => println!("Call to f1() failed")
            }
        }
        s2.take();
        s1.take();
        println!("Both domains are dead");
    }
}
