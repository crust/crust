use io::PortQueue;
use io::Result;
use super::act::Act;
use super::act::Own;
use super::Batch;
use super::packet_batch::PacketBatch;
use super::iterator::*;
use std::any::Any;
use  std::boxed::Box;

#[inline]
unsafe fn rdtsc() -> u64 {
    let mut low: u32;
    let mut high: u32;
    asm!("lfence
         rdtsc" : "={eax}" (low), "={edx}" (high));
         ((high as u64) << 32) | (low as u64)
}

#[inline]
unsafe fn rdtscp() -> u64 {
    let mut low: u32;
    let mut high: u32;
    asm!("rdtscp" : "={eax}" (low), "={edx}" (high) ::: "volatile");
    asm!("lfence"::::);
         ((high as u64) << 32) | (low as u64)
}


// FIXME: Should we be handling multiple queues and ports here?
pub struct ReceiveBatch {
    parent: PacketBatch,
    port: PortQueue,
    pub received: u64,
}

impl ReceiveBatch {
    pub fn new_with_parent(parent: PacketBatch, port: PortQueue) -> ReceiveBatch {
        ReceiveBatch {
            parent: parent,
            port: port,
            received: 0,
        }
    }

    pub fn new(port: PortQueue) -> ReceiveBatch {
        ReceiveBatch {
            //parent: PacketBatch::new(rx_cnt),
            parent: PacketBatch::new(32),
            port: port,
            received: 0,
        }

    }
}

impl Batch for ReceiveBatch {}

impl BatchIterator for ReceiveBatch {
    #[inline]
    fn start(&mut self) -> usize {
        self.parent.start()
    }

    #[inline]
    unsafe fn next_payload(&mut self, idx: usize) -> Option<(PacketDescriptor, Option<&mut Any>, usize)> {
        self.parent.next_payload(idx)
    }

    #[inline]
    unsafe fn next_base_payload(&mut self, idx: usize) -> Option<(PacketDescriptor, Option<&mut Any>, usize)> {
        self.parent.next_base_payload(idx)
    }

    #[inline]
    unsafe fn next_payload_popped(&mut self,
                                  idx: usize,
                                  pop: i32)
                                  -> Option<(PacketDescriptor, Option<&mut Any>, usize)> {
        self.parent.next_payload_popped(idx, pop)
    }
}

impl Own<PacketBatch> for ReceiveBatch {
    fn own_parent(self) -> PacketBatch {
        self.parent
    }
}

/// Internal interface for packets.
impl Act for ReceiveBatch {
    #[inline]
    fn parent(&mut self) -> &mut Batch {
        &mut self.parent
    }

    #[inline]
    fn parent_immutable(&self) -> &Batch {
        &self.parent
    }
    
//    #[inline]
//    fn parent_owned(self) -> PacketBatch {
//        self.parent
 //   }
    
    #[inline]
    fn act(&mut self) {
        //let mut a : u64 = 0;
        //let mut b : u64 = 0;
        
        //panic!("panic test");
        self.parent.act();
        //unsafe{ b = rdtsc()};
        self.parent
            .recv(&mut self.port)
            .and_then(|x| {
                self.received += x as u64;
                Ok(x)
            })
            .expect("Receive failed");
        //unsafe{ a = rdtscp()};
        //self.ts = a - b;
        //if self.received > 0 {
        //    println!("rcd {}", self.received);
       // }
    }

    #[inline]
    fn done(&mut self) {
        // Free up memory
        self.parent.deallocate_batch().expect("Deallocation failed");
    }

    #[inline]
    fn send_q(&mut self, port: &mut PortQueue) -> Result<u32> {
        self.parent.send_q(port)
    }

    #[inline]
    fn capacity(&self) -> i32 {
        self.parent.capacity()
    }

    #[inline]
    fn drop_packets(&mut self, idxes: &Vec<usize>) -> Option<usize> {
        self.parent.drop_packets(idxes)
    }

    #[inline]
    fn adjust_payload_size(&mut self, idx: usize, size: isize) -> Option<isize> {
        self.parent.adjust_payload_size(idx, size)
    }

    #[inline]
    fn adjust_headroom(&mut self, idx: usize, size: isize) -> Option<isize> {
        self.parent.adjust_headroom(idx, size)
    }
}
