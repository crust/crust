use io::PortQueue;
use io::Result;
use super::act::Act;
use super::Batch;
use super::iterator::*;
use std::any::Any;
use scheduler::Executable;
use  std::boxed::Box;

// FIXME: Should we be handling multiple queues and ports here?
// FIXME: Should this really even be a batch?

#[inline]
unsafe fn rdtsc() -> u64 {
    let mut low: u32;
    let mut high: u32;
    asm!("lfence
         rdtsc" : "={eax}" (low), "={edx}" (high));
      ((high as u64) << 32) | (low as u64)
}

#[inline]
unsafe fn rdtscp() -> u64 {
    let mut low: u32;
    let mut high: u32;
    asm!("rdtscp" : "={eax}" (low), "={edx}" (high) ::: "volatile");
     asm!("lfence"::::);
      ((high as u64) << 32) | (low as u64)
}

pub struct SendBatch<V>
    where V: Batch + BatchIterator + Act + 'static
{
    port: PortQueue,
    parent: V,
    pub sent: u64,
}

impl<V> SendBatch<V>
    where V: Batch + BatchIterator + Act
{
    pub fn new(parent: V, port: PortQueue) -> SendBatch<V> {
        SendBatch {
            port: port,
            sent: 0,
            parent: parent,
        }
    }
}

impl<V> Batch for SendBatch<V> where V: Batch + BatchIterator + Act {}

impl<V> BatchIterator for SendBatch<V>
    where V: Batch + BatchIterator + Act
{
    #[inline]
    fn start(&mut self) -> usize {
        panic!("Cannot iterate SendBatch")
    }

    #[inline]
    unsafe fn next_payload(&mut self, _: usize) -> Option<(PacketDescriptor, Option<&mut Any>, usize)> {
        panic!("Cannot iterate SendBatch")
    }

    #[inline]
    unsafe fn next_base_payload(&mut self, _: usize) -> Option<(PacketDescriptor, Option<&mut Any>, usize)> {
        panic!("Cannot iterate SendBatch")
    }

    #[inline]
    unsafe fn next_payload_popped(&mut self, _: usize, _: i32) -> Option<(PacketDescriptor, Option<&mut Any>, usize)> {
        panic!("Cannot iterate SendBatch")
    }
}

/// Internal interface for packets.
impl<V> Act for SendBatch<V>
    where V: Batch + BatchIterator + Act
{
    #[inline]
    fn parent(&mut self) -> &mut Batch {
        &mut self.parent
    }

    #[inline]
    fn parent_immutable(&self) -> &Batch {
        &self.parent
    }
    
//    #[inline]
//    fn parent_owned(self) -> V {
//        self.parent
//    }

    #[inline]
    fn act(&mut self) {
        // First everything is applied
        let mut b4 : u64 = 0;
        let mut  a4 : u64 = 0;
        let mut sent = 0;

        //println!("******************************** call send act");
        unsafe{ b4 = rdtsc(); }
        self.parent.act();
        unsafe { a4 = rdtscp(); }
        //println!("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ end send act");
        
        self.parent
            .send_q(&mut self.port)
            .and_then(|x| {
                sent = self.sent;
                self.sent += x as u64;
                if sent != self.sent {
                    //println!("cycles - {}", a4-b4);
                    //if self.cnt > 0 {
                        //println!("a4-b4 {} self.parent.dbg_ts() {}", a4-b4, self.parent.dbg_ts());
                        /*subtract the cost of recv() in rxb and cost of 2 rdtsc instructions */
                        //write!(self.buffer, "{}\n", (a4-b4));
                        //write!(self.buffer, "{}\n", (a4-b4));
                    //}
                    //self.cnt += 1;
                    //if sent >= ((x as u64) * 1001) {
                    //    panic!("drop everything")
                    //}
                }
                Ok(x)
            })
            .expect("Send failed");
        self.parent.done();
    }

    fn done(&mut self) {}
    
    fn send_q(&mut self, _: &mut PortQueue) -> Result<u32> {
        panic!("Cannot send a sent packet batch")
    }

    fn capacity(&self) -> i32 {
        self.parent.capacity()
    }

    #[inline]
    fn drop_packets(&mut self, _: &Vec<usize>) -> Option<usize> {
        panic!("Cannot drop packets from a sent batch")
    }

    #[inline]
    fn adjust_payload_size(&mut self, _: usize, _: isize) -> Option<isize> {
        panic!("Cannot resize a sent batch")
    }

    #[inline]
    fn adjust_headroom(&mut self, _: usize, _: isize) -> Option<isize> {
        panic!("Cannot resize a sent batch")
    }
}

impl<V> Executable for SendBatch<V>
    where V: Batch + BatchIterator + Act
{
    #[inline]
    fn execute(&mut self) {
        self.act()
    }
}
