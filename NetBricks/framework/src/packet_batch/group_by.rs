use headers::EndOffset;
use utils::{SpscConsumer, SpscProducer, new_spsc_queue};
use super::act::Act;
use super::Batch;
use scheduler::{Executable, Scheduler};
use super::ReceiveQueue;
use super::iterator::*;
use std::any::*;
use std::collections::HashMap;
use std::marker::PhantomData;
use std::ptr;

#[inline]
unsafe fn rdtsc() -> u64 {
    
    let mut low: u32;
    let mut high: u32;
    
    asm!("lfence
          rdtsc" : "={eax}" (low), "={edx}" (high));
       ((high as u64) << 32) | (low as u64)
}

#[inline]
unsafe fn rdtscp() -> u64 {
    
    let mut low: u32;
    let mut high: u32;
    
    asm!("rdtscp" : "={eax}" (low), "={edx}" (high) ::: "volatile");
    asm!("lfence"::::);
    ((high as u64) << 32) | (low as u64)
}

pub type GroupFn<T, S> = Box<FnMut(&mut T, &mut [u8], Option<&mut Any>) -> (usize, Option<Box<S>>) + Send>;

struct GroupByProducer<T, V, S>
    where T: EndOffset + 'static,
          V: Batch + BatchIterator + Act + 'static,
          S: 'static + Any + Default + Clone + Sized + Send
{
    parent: V,
    group_ct: usize,
    group_fn: GroupFn<T, S>,
    producers: Vec<SpscProducer<u8>>,
    groups: Vec<(usize, *mut u8)>,
}

pub struct GroupBy<T, V, S>
    where T: EndOffset + 'static,
          V: Batch + BatchIterator + Act + 'static,
          S: 'static + Any + Default + Clone + Sized + Send
{
    group_ct: usize,
    consumers: HashMap<usize, SpscConsumer<u8>>,
    phantom_t: PhantomData<T>,
    phantom_v: PhantomData<V>,
    phantom_s: PhantomData<S>,
}

impl<T, V, S> GroupBy<T, V, S>
    where T: EndOffset + 'static,
          V: Batch + BatchIterator + Act + 'static,
          S: 'static + Any + Default + Clone + Sized + Send
{
    pub fn new(parent: V, groups: usize, group_fn: GroupFn<T, S>, sched: &mut Scheduler) -> GroupBy<T, V, S> {
        let capacity = parent.capacity() as usize;
        let (producers, consumers) = {
            let mut producers = Vec::with_capacity(groups);
            let mut consumers = HashMap::with_capacity(groups);
            for i in 0..groups {
                let (prod, consumer) = new_spsc_queue(1 << 20).unwrap();
                producers.push(prod);
                consumers.insert(i, consumer);
            }
            (producers, consumers)
        };
        sched.add_task(GroupByProducer::<T, V, S> {
            parent: parent,
            group_ct: groups,
            group_fn: group_fn,
            producers: producers,
            groups: Vec::with_capacity(capacity),
        });
        GroupBy {
            group_ct: groups,
            consumers: consumers,
            phantom_t: PhantomData,
            phantom_v: PhantomData,
            phantom_s: PhantomData,
        }
    }

    #[inline]
    pub fn group_count(&self) -> usize {
        self.group_ct
    }

    #[inline]
    pub fn get_group(&mut self, group: usize) -> Option<ReceiveQueue<S>> {
        // FIXME: This currently loses all the parsing, we should fix it to not be the case.
        if group > self.group_ct {
            None
        } else {
            self.consumers
                .remove(&group)
                .and_then(|q| Some(ReceiveQueue::<S>::new(q)))
        }
    }

}

impl<T, V, S> Executable for GroupByProducer<T, V, S>
    where T: EndOffset + 'static,
          V: Batch + BatchIterator + Act + 'static,
          S: 'static + Any + Default + Clone + Sized + Send
{
    #[inline]
    fn execute(&mut self) {
        self.parent.act(); // Let the parent get some packets.
        {
            //let mut b4 : u64 = 0;
            //let mut  a4 : u64 = 0;
            
            //unsafe{ b4 = rdtsc(); }
            
            //let mut prcd = 0;

            //prcd = self.cnt;
            let iter = PayloadEnumerator::<T>::new(&mut self.parent);
            while let Some(ParsedDescriptor { header: hdr, payload, ctx, .. }) = iter.next(&mut self.parent) {
                let (group, meta) = (self.group_fn)(hdr, payload, ctx);
                //self.cnt += 1;
                self.groups.push((group,
                                  meta.and_then(|m| Some(Box::into_raw(m) as *mut u8))
                    .unwrap_or_else(|| ptr::null_mut())))
            }

            // At this time groups contains what we need to distribute, so distribute it out.
            self.parent.distribute_to_queues(&self.producers, &self.groups, self.group_ct);
            self.groups.clear();

            //unsafe{ a4 = rdtscp(); }
            
            //if prcd != self.cnt {
            //        if self.skip_first > 0 {
            //            write!(self.buffer, "{}\n", (a4-b4));
            //        }
            //        self.skip_first += 1;
            //}
        };
        self.parent.done();
    }
}
