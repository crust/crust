import csv
import os
import sys
import math

files = os.listdir(sys.argv[1])

for fil in files:
    print fil
    
for fil in files:
    fname = os.path.join(sys.argv[1], fil)
    with open(fname, 'rw') as csvfile:
        cycle = csv.reader(csvfile)
        lst = [int(row[0],10) for row in cycle]
        total = sum(lst)
        print total
        print len(lst)
        avg = total / len(lst)
        variance = sum([(e-avg)**2 for e in lst]) / len(lst)
        sd = math.sqrt(variance)
    with open(fname, "a") as wrf:
        wrf.write("Average: %s \n" % avg)
        wrf.write("SD: %s" % sd)
