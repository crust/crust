#![feature(box_syntax)]
extern crate protect;
extern crate e2d2;
extern crate time;
extern crate getopts;
extern crate rand;
use e2d2::io::*;
use e2d2::headers::*;
use e2d2::utils::*;
use e2d2::packet_batch::*;
use e2d2::scheduler::Executable;
use e2d2::state::*;
use getopts::Options;
use std::collections::HashMap;
use std::env;
use std::time::Duration;
use std::thread;
use std::panic;
use std::any::Any;
use std::sync::Arc;
use protect::protectb;
use protect::protectb_recoverable;
use std::fs::File;

const CONVERSION_FACTOR: f64 = 1000000000.;

fn construct_chain_no_pd<T: 'static + Batch>(parent: T) -> CompositionBatch {
    parent.compose()
}

//fn construct_chain_with_pd<T: 'static + Batch>(parent: T) -> CompositionBatch {
//    protect(|| parent.compose())
//}

fn monitor<T: 'static + Batch>(parent: T, mut monitoring_cache: MergeableStoreDP<isize>, no_nf: u32, pd: bool) -> CompositionBatch {
    
              /*parent.parse::<MacHeader>()
              .transform(move |hdr: &mut MacHeader, payload: &mut [u8], _: Option<&mut Any>| {
              // No one else should be writing to this, so I think
              // relaxed is safe here.
              let src = hdr.src.clone();
                    hdr.src = hdr.dst;
                    hdr.dst = src;
                    monitoring_cache.update(ipv4_extract_flow(hdr,payload).unwrap(), 1);
              })
              .parse::<IpHeader>()
              .transform(|hdr: &mut IpHeader, _: &mut [u8], _: Option<&mut Any>| {
                                                                 let ttl = hdr.ttl();
                                                                 hdr.set_ttl(ttl + 1)
                                                              })
              .compose()*/

            /*let prb = protectb(|| parent.compose());
           prb.compose()*/

            /* No protection domains */

            if !pd {
                let mut chain = construct_chain_no_pd(parent);
                for _ in 1..no_nf {
                    chain = construct_chain_no_pd(chain);
                }
                
                return chain.compose();

            } else {
                let mut chain = protectb(|| parent.compose());
                for _ in 1..no_nf {
                    chain = protectb(|| chain.compose());
                }
                return chain.compose(); 
            }
}

fn handle_work(ports: Vec<PortQueue>, core: i32, counter: MergeableStoreDP<isize>, 
               work_type: &str, rx_pkt: i32, no_nf: u32, pd: bool) {
    
    println!("Handle work - {} with {} packets", work_type, rx_pkt);

       
    let pipelines: Vec<_> = ports.iter()
                                 .map(|port| {
                                    let ctr = counter.clone();
                                    let mut fname = String::from(" ");
    
                                    if pd {
                                        fname = format!("output/pd/{}-NF-{}-pkt-{}PD.csv",no_nf, rx_pkt,"with");
                                    } else {
                                        fname = format!("output/nopd/{}-NF-{}-pkt-{}PD.csv",no_nf, rx_pkt,"no");
                                    }

                                    let mut buffer = File::create(fname).unwrap();
                                    let p = port.clone();
                                        monitor(ReceiveBatch::new(p, rx_pkt), ctr, no_nf, pd)
                                        .send(port.clone(), buffer)
                                        .compose()
                                 })
                                 .collect();
    
    println!("Running {} pipelines", pipelines.len());
    let mut combined = merge(pipelines);
    
    let ret = panic::catch_unwind(panic::AssertUnwindSafe(||
        loop {
            combined.execute();
        }
    ));
    
    if ret.is_err() {
        println!("End of work - {} with {} packets", work_type, rx_pkt);
    }
}

fn recv_thread(ports: Vec<PortQueue>, core: i32, counter: MergeableStoreDP<isize>) {
    init_thread(core, core);
    
    let pd : bool = true;

    println!("Receiving started");
    handle_work(ports.clone(), core, counter.clone(), "Single NF No PD", 1, 1, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Single NF No PD", 2, 1, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Single NF No PD", 4, 1, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Single NF No PD", 8, 1, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Single NF No PD", 16, 1, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Single NF No PD", 32, 1, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Single NF No PD", 64, 1, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Single NF No PD", 128, 1, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Single NF No PD", 256, 1, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Single NF No PD", 512, 1, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Single NF No PD", 1024, 1, !pd);
    
    handle_work(ports.clone(), core, counter.clone(), "Three NFs No PD", 1, 3, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Three NFs No PD", 2, 3, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Three NFs No PD", 4, 3, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Three NFs No PD", 8, 3, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Three NFs No PD", 16, 3, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Three NFs No PD", 32, 3, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Three NFs No PD", 64, 3, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Three NFs No PD", 128, 3, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Three NFs No PD", 256, 3, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Three NFs No PD", 512, 3, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Three NFs No PD", 1024, 3, !pd);

    handle_work(ports.clone(), core, counter.clone(), "Five NFs No PD", 1, 5, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Five NFs No PD", 2, 5, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Five NFs No PD", 4, 5, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Five NFs No PD", 8, 5, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Five NFs No PD", 16, 5, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Five NFs No PD", 32, 5, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Five NFs No PD", 64, 5, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Five NFs No PD", 128, 5, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Five NFs No PD", 256, 5, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Five NFs No PD", 512, 5, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Five NFs No PD", 1024, 5, !pd);
    
    handle_work(ports.clone(), core, counter.clone(), "Seven NFs No PD", 1, 7, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Seven NFs No PD", 2, 7, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Seven NFs No PD", 4, 7, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Seven NFs No PD", 8, 7, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Seven NFs No PD", 16, 7, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Seven NFs No PD", 32, 7, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Seven NFs No PD", 64, 7, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Seven NFs No PD", 128, 7, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Seven NFs No PD", 256, 7, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Seven NFs No PD", 512, 7, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Seven NFs No PD", 1024, 7, !pd);

    handle_work(ports.clone(), core, counter.clone(), "Nine NFs No PD", 1, 9, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Nine NFs No PD", 2, 9, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Nine NFs No PD", 4, 9, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Nine NFs No PD", 8, 9, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Nine NFs No PD", 16, 9, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Nine NFs No PD", 32, 9, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Nine NFs No PD", 64, 9, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Nine NFs No PD", 128, 9, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Nine NFs No PD", 256, 9, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Nine NFs No PD", 512, 9, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Nine NFs No PD", 1024, 9, !pd);
    
    handle_work(ports.clone(), core, counter.clone(), "Eleven NFs No PD", 1, 11, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Eleven NFs No PD", 2, 11, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Eleven NFs No PD", 4, 11, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Eleven NFs No PD", 8, 11, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Eleven NFs No PD", 16, 11, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Eleven NFs No PD", 32, 11, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Eleven NFs No PD", 64, 11, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Eleven NFs No PD", 128, 11, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Eleven NFs No PD", 256, 11, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Eleven NFs No PD", 512, 11, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Eleven NFs No PD", 1024, 11, !pd);
    
    handle_work(ports.clone(), core, counter.clone(), "Thirteen NFs No PD", 1, 13, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Thirteen NFs No PD", 2, 13, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Thirteen NFs No PD", 4, 13, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Thirteen NFs No PD", 8, 13, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Thirteen NFs No PD", 16, 13, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Thirteen NFs No PD", 32, 13, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Thirteen NFs No PD", 64, 13, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Thirteen NFs No PD", 128, 13, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Thirteen NFs No PD", 256, 13, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Thirteen NFs No PD", 512, 13, !pd);
    handle_work(ports.clone(), core, counter.clone(), "Thirteen NFs No PD", 1024, 13, !pd);

    println!("Receiving started for protection domain");
    handle_work(ports.clone(), core, counter.clone(), "Single NF with PD", 1, 1, pd);
    handle_work(ports.clone(), core, counter.clone(), "Single NF with PD", 2, 1, pd);
    handle_work(ports.clone(), core, counter.clone(), "Single NF with PD", 4, 1, pd);
    handle_work(ports.clone(), core, counter.clone(), "Single NF with PD", 8, 1, pd);
    handle_work(ports.clone(), core, counter.clone(), "Single NF with PD", 16, 1, pd);
    handle_work(ports.clone(), core, counter.clone(), "Single NF with PD", 32, 1, pd);
    handle_work(ports.clone(), core, counter.clone(), "Single NF with PD", 64, 1, pd);
    handle_work(ports.clone(), core, counter.clone(), "Single NF with PD", 128, 1, pd);
    handle_work(ports.clone(), core, counter.clone(), "Single NF with PD", 256, 1, pd);
    handle_work(ports.clone(), core, counter.clone(), "Single NF with PD", 512, 1, pd);
    handle_work(ports.clone(), core, counter.clone(), "Single NF with PD", 1024, 1, pd);
    
    handle_work(ports.clone(), core, counter.clone(), "Three NFs with PD", 1, 3, pd);
    handle_work(ports.clone(), core, counter.clone(), "Three NFs with PD", 2, 3, pd);
    handle_work(ports.clone(), core, counter.clone(), "Three NFs with PD", 4, 3, pd);
    handle_work(ports.clone(), core, counter.clone(), "Three NFs with PD", 8, 3, pd);
    handle_work(ports.clone(), core, counter.clone(), "Three NFs with PD", 16, 3, pd);
    handle_work(ports.clone(), core, counter.clone(), "Three NFs with PD", 32, 3, pd);
    handle_work(ports.clone(), core, counter.clone(), "Three NFs with PD", 64, 3, pd);
    handle_work(ports.clone(), core, counter.clone(), "Three NFs with PD", 128, 3, pd);
    handle_work(ports.clone(), core, counter.clone(), "Three NFs with PD", 256, 3, pd);
    handle_work(ports.clone(), core, counter.clone(), "Three NFs with PD", 512, 3, pd);
    handle_work(ports.clone(), core, counter.clone(), "Three NFs with PD", 1024, 3, pd);

    handle_work(ports.clone(), core, counter.clone(), "Five NFs with PD", 1, 5, pd);
    handle_work(ports.clone(), core, counter.clone(), "Five NFs with PD", 2, 5, pd);
    handle_work(ports.clone(), core, counter.clone(), "Five NFs with PD", 4, 5, pd);
    handle_work(ports.clone(), core, counter.clone(), "Five NFs with PD", 8, 5, pd);
    handle_work(ports.clone(), core, counter.clone(), "Five NFs with PD", 16, 5, pd);
    handle_work(ports.clone(), core, counter.clone(), "Five NFs with PD", 32, 5, pd);
    handle_work(ports.clone(), core, counter.clone(), "Five NFs with PD", 64, 5, pd);
    handle_work(ports.clone(), core, counter.clone(), "Five NFs with PD", 128, 5, pd);
    handle_work(ports.clone(), core, counter.clone(), "Five NFs with PD", 256, 5, pd);
    handle_work(ports.clone(), core, counter.clone(), "Five NFs with PD", 512, 5, pd);
    handle_work(ports.clone(), core, counter.clone(), "Five NFs with PD", 1024, 5, pd);
    
    handle_work(ports.clone(), core, counter.clone(), "Seven NFs with PD", 1, 7, pd);
    handle_work(ports.clone(), core, counter.clone(), "Seven NFs with PD", 2, 7, pd);
    handle_work(ports.clone(), core, counter.clone(), "Seven NFs with PD", 4, 7, pd);
    handle_work(ports.clone(), core, counter.clone(), "Seven NFs with PD", 8, 7, pd);
    handle_work(ports.clone(), core, counter.clone(), "Seven NFs with PD", 16, 7, pd);
    handle_work(ports.clone(), core, counter.clone(), "Seven NFs with PD", 32, 7, pd);
    handle_work(ports.clone(), core, counter.clone(), "Seven NFs with PD", 64, 7, pd);
    handle_work(ports.clone(), core, counter.clone(), "Seven NFs with PD", 128, 7, pd);
    handle_work(ports.clone(), core, counter.clone(), "Seven NFs with PD", 256, 7, pd);
    handle_work(ports.clone(), core, counter.clone(), "Seven NFs with PD", 512, 7, pd);
    handle_work(ports.clone(), core, counter.clone(), "Seven NFs with PD", 1024, 7, pd);

    handle_work(ports.clone(), core, counter.clone(), "Nine NFs with PD", 1, 9, pd);
    handle_work(ports.clone(), core, counter.clone(), "Nine NFs with PD", 2, 9, pd);
    handle_work(ports.clone(), core, counter.clone(), "Nine NFs with PD", 4, 9, pd);
    handle_work(ports.clone(), core, counter.clone(), "Nine NFs with PD", 8, 9, pd);
    handle_work(ports.clone(), core, counter.clone(), "Nine NFs with PD", 16, 9, pd);
    handle_work(ports.clone(), core, counter.clone(), "Nine NFs with PD", 32, 9, pd);
    handle_work(ports.clone(), core, counter.clone(), "Nine NFs with PD", 64, 9, pd);
    handle_work(ports.clone(), core, counter.clone(), "Nine NFs with PD", 128, 9, pd);
    handle_work(ports.clone(), core, counter.clone(), "Nine NFs with PD", 256, 9, pd);
    handle_work(ports.clone(), core, counter.clone(), "Nine NFs with PD", 512, 9, pd);
    handle_work(ports.clone(), core, counter.clone(), "Nine NFs with PD", 1024, 9, pd);
    
    handle_work(ports.clone(), core, counter.clone(), "Eleven NFs with PD", 1, 11, pd);
    handle_work(ports.clone(), core, counter.clone(), "Eleven NFs with PD", 2, 11, pd);
    handle_work(ports.clone(), core, counter.clone(), "Eleven NFs with PD", 4, 11, pd);
    handle_work(ports.clone(), core, counter.clone(), "Eleven NFs with PD", 8, 11, pd);
    handle_work(ports.clone(), core, counter.clone(), "Eleven NFs with PD", 16, 11, pd);
    handle_work(ports.clone(), core, counter.clone(), "Eleven NFs with PD", 32, 11, pd);
    handle_work(ports.clone(), core, counter.clone(), "Eleven NFs with PD", 64, 11, pd);
    handle_work(ports.clone(), core, counter.clone(), "Eleven NFs with PD", 128, 11, pd);
    handle_work(ports.clone(), core, counter.clone(), "Eleven NFs with PD", 256, 11, pd);
    handle_work(ports.clone(), core, counter.clone(), "Eleven NFs with PD", 512, 11, pd);
    handle_work(ports.clone(), core, counter.clone(), "Eleven NFs with PD", 1024, 11, pd);
    
    handle_work(ports.clone(), core, counter.clone(), "Thirteen NFs with PD", 1, 13, pd);
    handle_work(ports.clone(), core, counter.clone(), "Thirteen NFs with PD", 2, 13, pd);
    handle_work(ports.clone(), core, counter.clone(), "Thirteen NFs with PD", 4, 13, pd);
    handle_work(ports.clone(), core, counter.clone(), "Thirteen NFs with PD", 8, 13, pd);
    handle_work(ports.clone(), core, counter.clone(), "Thirteen NFs with PD", 16, 13, pd);
    handle_work(ports.clone(), core, counter.clone(), "Thirteen NFs with PD", 32, 13, pd);
    handle_work(ports.clone(), core, counter.clone(), "Thirteen NFs with PD", 64, 13, pd);
    handle_work(ports.clone(), core, counter.clone(), "Thirteen NFs with PD", 128, 13, pd);
    handle_work(ports.clone(), core, counter.clone(), "Thirteen NFs with PD", 256, 13, pd);
    handle_work(ports.clone(), core, counter.clone(), "Thirteen NFs with PD", 512, 13, pd);
    handle_work(ports.clone(), core, counter.clone(), "Thirteen NFs with PD", 1024, 13, pd);

}

fn main() {
    let args: Vec<String> = env::args().collect();
    let program = args[0].clone();
    let mut opts = Options::new();
    opts.optflag("h", "help", "print this help menu");
    opts.optflag("", "secondary", "run as a secondary process");
    opts.optopt("n", "name", "name to use for the current process", "name");
    opts.optmulti("p", "port", "Port to use", "[type:]id");
    opts.optmulti("c", "core", "Core to use", "core");
    opts.optopt("m", "master", "Master core", "master");
    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(f) => panic!(f.to_string()),
    };
    if matches.opt_present("h") {
        print!("{}", opts.usage(&format!("Usage: {} [options]", program)));
    }

    let cores_str = matches.opt_strs("c");
    let master_core = matches.opt_str("m")
                             .unwrap_or_else(|| String::from("0"))
                             .parse()
                             .expect("Could not parse master core spec");
    println!("Using master core {}", master_core);
    let name = matches.opt_str("n").unwrap_or_else(|| String::from("recv"));

    let cores: Vec<i32> = cores_str.iter()
                                   .map(|n: &String| n.parse().ok().expect(&format!("Core cannot be parsed {}", n)))
                                   .collect();


    fn extract_cores_for_port(ports: &[String], cores: &[i32]) -> HashMap<String, Vec<i32>> {
        let mut cores_for_port = HashMap::<String, Vec<i32>>::new();
        for (port, core) in ports.iter().zip(cores.iter()) {
            cores_for_port.entry(port.clone()).or_insert(vec![]).push(*core)
        }
        cores_for_port
    }

    let primary = !matches.opt_present("secondary");

    let cores_for_port = extract_cores_for_port(&matches.opt_strs("p"), &cores);

    if primary {
        init_system_wl(&name, master_core, &[]);
    } else {
        init_system_secondary(&name, master_core);
    }

    let ports_to_activate: Vec<_> = cores_for_port.keys().collect();

    let mut queues_by_core = HashMap::<i32, Vec<_>>::with_capacity(cores.len());
    let mut ports = Vec::<Arc<PmdPort>>::with_capacity(ports_to_activate.len());
    for port in &ports_to_activate {
        let cores = cores_for_port.get(*port).unwrap();
        let queues = cores.len() as i32;
        let pmd_port = PmdPort::new_with_queues(*port, queues, queues, cores, cores)
                               .expect("Could not initialize port");
        for (idx, core) in cores.iter().enumerate() {
            let queue = idx as i32;
            queues_by_core.entry(*core)
                          .or_insert(vec![])
                          .push(PmdPort::new_queue_pair(&pmd_port, queue, queue).unwrap());
        }
        ports.push(pmd_port);
    }

    const _BATCH: usize = 1 << 10;
    const _CHANNEL_SIZE: usize = 256;
    let mut consumer = MergeableStoreCP::new();
    let _thread: Vec<_> = queues_by_core.iter()
                                       .map(|(core, ports)| {
                                           let c = core.clone();
                                           let mon = consumer.dp_store();
                                           let p: Vec<_> = ports.iter().map(|p| p.clone()).collect();
                                           std::thread::spawn(move || recv_thread(p, c, mon))
                                       })
                                       .collect();
    let mut pkts_so_far = (0, 0);
    let mut start = time::precise_time_ns() as f64 / CONVERSION_FACTOR;
    let sleep_time = Duration::from_millis(500);
    loop {
        thread::sleep(sleep_time); // Sleep for a bit
        consumer.sync();
        let now = time::precise_time_ns() as f64 / CONVERSION_FACTOR;
        if now - start > 1.0 {
            let mut rx = 0;
            let mut tx = 0;
            for port in &ports {
                for q in 0..port.rxqs() {
                    let (rp, tp) = port.stats(q);
                    rx += rp;
                    tx += tp;
                }
            }
            let pkts = (rx, tx);
            println!("{:.2} OVERALL RX {:.2} TX {:.2} FLOWS {}",
                     now - start,
                     (pkts.0 - pkts_so_far.0) as f64 / (now - start),
                     (pkts.1 - pkts_so_far.1) as f64 / (now - start),
                     consumer.len());
            start = now;
            pkts_so_far = pkts;
        }
    }
}
