use e2d2::packet_batch::*;
use e2d2::packet_batch::act::Act;
use e2d2::packet_batch::act::Own;
use e2d2::packet_batch::iterator::BatchIterator;
use e2d2::packet_batch::iterator::PacketDescriptor;
use e2d2::io::Result;
use e2d2::io::PortQueue;
use std::marker::PhantomData;
use e2d2::utils::SpscProducer;
use e2d2::headers::EndOffset;
use domain::*;
use std::any::Any;
use std::sync::{Arc, Mutex,};


pub struct ProtectBatch<T,P>
    where T : Batch + Own<P> +'static,
          P : Batch + 'static
{
        domain : Arc<Mutex<Domain>>,
        nf : RRef<T>,
        rec_info : Option<Box<FnMut(P) -> T + Send>>,
        phantom_t: PhantomData<P>,
        recovery_on : bool
}

impl<T,P> Drop for ProtectBatch<T,P> 
    where T : Batch + Own<P>,
        P : Batch + 'static
{
    fn drop(&mut self) {
        //println!("drop protectbatch");
        drop(&mut self.nf);
        drop(&mut self.domain);
    }
}

impl<T,P> ProtectBatch<T,P>
    where T : Batch + Own<P>,
          P : Batch + 'static
{
    fn cleanup(&self) {
        drop(&self.domain);
    }

    fn get_parent_nf(&mut self) -> P {
  
        let r = self.nf.call_owned(|batch| {
            Ok(batch.own_parent())
        },0, self.recovery_on);

        match r {
            Ok(r) => r,
            Err(_) => panic!("[Protect:Err] call to parent_owned 
                      failed"),
        }
    }

    fn recover_nf(&mut self) {

        let parent = self.get_parent_nf();
        let dom = Domain::new();
       
        println!("Recovery begins...");
        let mut rinfo = match self.rec_info.as_mut() {
            Some(r) => r,
            None => panic!("cannot obtain recovery info"),
        }; 

        let r = Domain::execute(&dom, move || {
             let batch = rinfo(parent);
             let rref = RRef::new(batch);
             Ok(rref)
        });
    
        match r {
            Ok(rref) => {
                self.domain = dom;
                self.nf = rref;
            }
            Err(_) => {
                panic!("Failed to create a protected batch");
            }
        }
        println!("Protectbatch patched with new chain!")
   }
}

/* protect function that should be called 
 * to wrap functionality within a protection 
 * domain */
pub fn protectb<T, F, P>(f : F) -> ProtectBatch<T, P>
    where T : Batch + Own<P>, 
        F : FnOnce() -> T,
        P : Batch + 'static
{
    let dom = Domain::new();

    let r = Domain::execute(&dom, || {
        let batch = f();
        let rref = RRef::new(batch);
        Ok(rref)
    });

    match r {
        Ok(rref) => {
            ProtectBatch {
                domain : dom, 
                nf : rref,
                rec_info : None,
                phantom_t: PhantomData,
                recovery_on: false,
            }
        }
        Err(_) => {
            panic!("Failed to create a protected batch")
        }
    }
    
}


/* Recoverable variant of protectb */
pub fn protectb_recoverable<T, F, P>(p : P, mut f : F) -> ProtectBatch<T, P>
    where T : Batch + Own<P>,
          F : FnMut(P) -> T + Send + 'static,
          P : Batch + 'static
{
    let dom = Domain::new();

    let r = Domain::execute(&dom, || {
        let batch = f(p);
        let rref = RRef::new(batch);
        Ok(rref)
    });

    match r {
        Ok(rref) => {
            ProtectBatch {
                domain : dom, 
                nf : rref,
                rec_info : Some(Box::new(f)),
                phantom_t: PhantomData,
                recovery_on: true,
            }
        }
        Err(_) => {
            panic!("Failed to create a protected batch")
        }
    }
}

impl<T,P> Own<P> for ProtectBatch<T, P> 
    where T : Batch + Own<P>, 
          P : Batch + 'static 
{
    fn own_parent(self) -> P {
        panic!("ProtectBatch doesn't implement own_parent")
    }
}

impl<T,P> Batch for ProtectBatch<T, P> 
    where T : Batch + Own<P>, 
          P:  Batch + 'static + Send {}

impl<T, P> Act for ProtectBatch<T, P> 
        where T : Batch + Own<P>,
              P : Batch +  'static
{
    #[inline]
    fn parent(&mut self) -> &mut Batch {
        panic!("ProtectBatch doesn't expose a reference 
                            to object wrapped inside");
    }

    #[inline]
    fn parent_immutable(&self) -> &Batch {
         panic!("ProtectBatch doesn't expose a reference 
                            to object wrapped inside");
    }
 
    #[inline]
    fn act(&mut self) {
        //println!("PDact");
        let r = self.nf.call(|batch| {
            batch.act();
            Ok(())
        }, 1, self.recovery_on);

        match r {
            Ok(_) => {},
            Err(_) => {
                println!("callin cleanup");
                //self.recover_nf();
                //self.cleanup();
                println!("[Protect:Err] call to act failed")
            }
        }
    }
 
    #[inline]
    fn done(&mut self) {
        //println!("PDdone");
        let r = self.nf.call(|batch| {
            batch.done();
            Ok(())
        }, 2, self.recovery_on);
 
        match r {
            Ok(_) => {},
            Err(_) => println!("[Protect:Err] call to act 
                                failed"),
        }
   }
    
    #[inline]
    fn capacity(&self) -> i32 {
        //println!("PDcap");
        let c = self.nf.call_immut(|batch| {
            let c = batch.capacity();
            Ok(c)
        }, 3, self.recovery_on);

        match c {
            Ok(c) => c,
            Err(_) => panic!("[Protect:Err] call to 
                                capacity failed"),
        }
    }

    #[inline]
    fn send_q(&mut self, pq : &mut PortQueue) -> Result<u32> {

        //println!("PDsendq");
        let r = self.nf.call(|batch| {
            Ok(batch.send_q(pq))
        }, 4, self.recovery_on);
        
        match r {
            Ok(r) => r,
            Err(_) => panic!("[Protect:Err] call to
                             send_q failed"),
        }
    }

    #[inline]
    fn drop_packets(&mut self, idxes: &Vec<usize>) -> Option<usize> {
        
        //println!("PDdroppckt");
        let r = self.nf.call(|batch| {
            Ok(batch.drop_packets(idxes))
        }, 5, self.recovery_on);

        match r {
            Ok(r) => r,
            Err(_) => panic!("[Protect:Err] call to
                            drop_packets failed"),
        }
    }

    #[inline]
    fn adjust_payload_size(&mut self, idx: usize, size: isize) -> Option<isize> {
        
        //println!("PDadjpayload");
        let r = self.nf.call(|batch| {
            Ok(batch.adjust_payload_size(idx, size))
        }, 6, self.recovery_on);

        match r {
            Ok(r) => r,
            Err(_) => panic!("[Protect:Err] call to
                            adjust_payload_size failed"),
        }
    }
    
    #[inline]
    fn adjust_headroom(&mut self, idx: usize, size: isize) -> Option<isize> {
 
        //println!("PDadjheadroom");
        let r = self.nf.call(|batch| {
            Ok(batch.adjust_headroom(idx, size))
        }, 7, self.recovery_on);

        match r {
            Ok(r) => r,
            Err(_) => panic!("[Protect:Err] call to
                            adjust_payload_size failed"),
        }
    }

    #[inline]
    fn distribute_to_queues(&mut self, queues: &[SpscProducer<u8>], groups: &Vec<(usize, *mut u8)>, ngroups: usize) {
        //println!("distbqueue");
        let r = self.nf.call(|batch| {
                batch.distribute_to_queues(queues, groups, ngroups);
            Ok(())
        }, 8, self.recovery_on);
        
        match r {
            Ok(_) => {},
            Err(_) => panic!("[Protect:Err] call to
                        distribute_to_queues failed"),
        }
    }
}

impl<T, P> BatchIterator for ProtectBatch<T, P> 
    where T : Batch + Own<P>,
          P : Batch + 'static
{
    #[inline]
    fn start(&mut self) -> usize {
 
       // println!("PDstart");
        let r = self.nf.call(|batch| {
            Ok(batch.start())
        }, 9, self.recovery_on);

        match r {
            Ok(r) => r,
            Err(_) => panic!("[Protect:Err] call to
                            start failed"),
        }
    }

    #[inline]   
    unsafe fn next_payload(&mut self, idx: usize) -> Option<(PacketDescriptor, Option<&mut Any>, usize)> {

        //println!("PDnxtpaylod");
        let r = self.nf.call(|batch| {
            match batch.next_payload(idx) {
                Some((descriptor, _, ret)) => {
                    Ok(Some((descriptor, None, ret)))
                    //Ok(Some((descriptor,
                      //       opt.and_then(|x| { Some(x as usize) }),
                      //       ret)))
                },
                None => Ok(None),
            }
        }, 10, self.recovery_on);

        match r {
            Ok(r) => r,
            Err(_) => panic!("[Protect:Err] call to
                            next_payload failed"),
        }
   }

    #[inline]
    unsafe fn next_base_payload(&mut self, idx: usize) -> Option<(PacketDescriptor, Option<&mut Any>, usize)> {
        //println!("PDnxtbasepaylod");
        let r = self.nf.call(|batch| {
            match batch.next_base_payload(idx) {
                Some((descriptor, _, ret)) => {
                    Ok(Some((descriptor, None, ret)))
                    //Ok(Some((descriptor,
                      //       opt.and_then(|x| { Some(x as usize) }),
                      //       ret)))
                },
                None => Ok(None),
            }
        }, 11, self.recovery_on);

        match r {
            Ok(r) => r,
            Err(_) => panic!("[Protect:Err] call to
                            next_base_payload failed"),
        }
    }

    #[inline]
    unsafe fn next_payload_popped(&mut self, idx: usize, pop: i32) -> Option<(PacketDescriptor, Option<&mut Any>, usize)> {
        
        //println!("PDnxtpaylodpopped");
        let r = self.nf.call(|batch| {
            match batch.next_payload_popped(idx, pop) {
                Some((descriptor, _, ret)) => {
                    Ok(Some((descriptor, None, ret)))
                    //Ok(Some((descriptor,
                      //       opt.and_then(|x| { Some(x as usize) }),
                      //       ret)))
                },
                None => Ok(None),
            }
        }, 12, self.recovery_on);

        match r {
            Ok(r) => r,
            Err(_) => panic!("[Protect:Err] call to
                            next_payload_popped failed"),
        }
    }

}
