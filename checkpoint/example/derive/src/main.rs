#![feature(plugin, custom_attribute, custom_derive)]
#![plugin(cpplugin)]

//extern crate checkpoint;
//use checkpoint::CheckPoint;

trait CheckPoint {
    fn checkpt(&self) -> Self;    
}

#[derive(CheckPoint)]
struct Global {
    x : u32,
    y : u32,
}

fn main() {
    
    let inst = Global {x : 1, y : 1};
    println!("x {} and y {}", inst.x, inst.y);
    
    let cloned = inst.checkpt();
    println!("x {} and y {}", cloned.x, cloned.y);
    println!("x {} and y {}", inst.x, inst.y);
}
