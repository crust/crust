extern crate checkpoint;
use checkpoint::CheckPoint;
extern crate cparc;
use cparc::*;

fn org(mut x : &mut CpArc<u32>) {
    *CpArc::get_mut(&mut x).unwrap() = 4;
}

fn check(y : CpArc<u32>) {
    assert_eq!(*y.unwrap_cpdata(), 4);
}

fn main() {
    
    let mut a = CpArc::new(10);

    let c = a.checkpt();

    org(&mut a);
    check(c);
}
