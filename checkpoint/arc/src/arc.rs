#![feature(box_syntax)]
#![feature(dropck_parametricity)]
#![feature(core_intrinsics)]
#![feature(heap_api)]
#![feature(shared)]
#![feature(coerce_unsized)]
#![feature(unsize)]
#![feature(alloc)]
// Copyright 2012-2014 The Rust Project Developers. See the COPYRIGHT
// file at the top-level directory of this distribution and at
// http://rust-lang.org/COPYRIGHT.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Threadsafe reference-counted boxes (the `CpArc<T>` type).
//!
//! The `CpArc<T>` type provides shared ownership of an immutable value through
//! atomic reference counting.
//!
//! `Weak<T>` is a weak reference to the `CpArc<T>` box, and it is created by
//! the `downgrade` method.
//! # Examples
//!
//! Sharing some immutable data between threads:
//!
// Note that we **do not** run these tests here. The windows builders get super
// unhappy of a thread outlives the main thread and then exits at the same time
// (something deadlocks) so we just avoid this entirely by not running these
// tests.
//! ```no_run
//! use std::sync::CpArc;
//! use std::thread;
//!
//! let five = CpArc::new(5);
//!
//! for _ in 0..10 {
//!     let five = five.clone();
//!
//!     thread::spawn(move || {
//!         println!("{:?}", five);
//!     });
//! }
//! ```

extern crate alloc;
extern crate checkpoint;
use checkpoint::CheckPoint;
use std::boxed::Box;

use std::sync::atomic;
use std::sync::atomic::Ordering::{Acquire, Relaxed, Release, SeqCst};
use std::borrow;
use std::fmt;
use std::cmp::Ordering;
use std::mem::{align_of_val, size_of_val};
use std::intrinsics::abort;
use std::mem;
use std::mem::uninitialized;
use std::ops::Deref;
use std::ops::CoerceUnsized;
use std::ptr::{self, Shared};
use std::marker::Unsize;
use std::hash::{Hash, Hasher};
use std::{isize, usize};
use std::convert::From;
use alloc::heap::deallocate;

const MAX_REFCOUNT: usize = (isize::MAX) as usize;

/// An atomically reference counted wrapper for shared state.
/// Destruction is deterministic, and will occur as soon as the last owner is
/// gone. It is marked as `Send` because it uses atomic reference counting.
///
/// If you do not need thread-safety, and just need shared ownership, consider
/// the [`Rc<T>` type](../rc/struct.Rc.html). It is the same as `CpArc<T>`, but
/// does not use atomics, making it both thread-unsafe as well as significantly
/// faster when updating the reference count.
///
/// # Examples
///
/// In this example, a large vector of data will be shared by several threads. First we
/// wrap it with a `CpArc::new` and then clone the `CpArc<T>` reference for every thread (which will
/// increase the reference count atomically).
///
/// ```
/// use std::sync::CpArc;
/// use std::thread;
///
/// fn main() {
///     let numbers: Vec<_> = (0..100).collect();
///     let shared_numbers = CpArc::new(numbers);
///
///     for _ in 0..10 {
///         // prepare a copy of reference here and it will be moved to the thread
///         let child_numbers = shared_numbers.clone();
///
///         thread::spawn(move || {
///             let local_numbers = &child_numbers[..];
///
///             // Work with the local numbers
///         });
///     }
/// }
/// ```
/// You can also share mutable data between threads safely
/// by putting it inside `Mutex` and then share `Mutex` immutably
/// with `CpArc<T>` as shown below.
///
// See comment at the top of this file for why the test is no_run
/// ```no_run
/// use std::sync::{CpArc, Mutex};
/// use std::thread;
///
/// let five = CpArc::new(Mutex::new(5));
///
/// for _ in 0..10 {
///     let five = five.clone();
///
///     thread::spawn(move || {
///         let mut number = five.lock().unwrap();
///
///         *number += 1;
///
///         println!("{}", *number); // prints 6
///     });
/// }
/// ```

#[cfg_attr(stage0, unsafe_no_drop_flag)]
pub struct CpArc<T: ?Sized> {
    ptr: Shared<ArcInner<T>>,
    checkpointed : bool,
}

unsafe impl<T: ?Sized + Sync + Send> Send for CpArc<T> {}
unsafe impl<T: ?Sized + Sync + Send> Sync for CpArc<T> {}

//impl<T: ?Sized + Unsize<U>, U: ?Sized> CoerceUnsized<CpArc<U>> for CpArc<T> {}

/// A weak pointer to an `CpArc`.
///
/// Weak pointers will not keep the data inside of the `CpArc` alive, and can be
/// used to break cycles between `CpArc` pointers.
///
/// A `Weak<T>` pointer can be upgraded to an `CpArc<T>` pointer, but
/// will return `None` if the value has already been dropped.
///
/// For example, a tree with parent pointers can be represented by putting the
/// nodes behind strong `CpArc<T>` pointers, and then storing the parent pointers
/// as `Weak<T>` pointers.

#[cfg_attr(stage0, unsafe_no_drop_flag)]
pub struct Weak<T: ?Sized> {
    ptr: Shared<ArcInner<T>>,
}

unsafe impl<T: ?Sized + Sync + Send> Send for Weak<T> {}
unsafe impl<T: ?Sized + Sync + Send> Sync for Weak<T> {}

//impl<T: ?Sized + Unsize<U>, U: ?Sized> CoerceUnsized<Weak<U>> for Weak<T> {}

impl<T: ?Sized + fmt::Debug> fmt::Debug for Weak<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "(Weak)")
    }
}

struct ArcInner<T: ?Sized> {
    cpdata: Option<Box<T>>,
    strong: atomic::AtomicUsize,

    // the value usize::MAX acts as a sentinel for temporarily "locking" the
    // ability to upgrade weak pointers or downgrade strong ones; this is used
    // to avoid races in `make_mut` and `get_mut`.
    weak: atomic::AtomicUsize,
    cpref: atomic::AtomicUsize,
    total: atomic::AtomicUsize,

    data: T,
}

unsafe impl<T: ?Sized + Sync + Send> Send for ArcInner<T> {}
unsafe impl<T: ?Sized + Sync + Send> Sync for ArcInner<T> {}

impl<T> CpArc<T> {
    /// Constructs a new `CpArc<T>`.
    ///
    /// # Examples
    ///
    /// ```
    /// use std::sync::CpArc;
    ///
    /// let five = CpArc::new(5);
    /// ```
    #[inline]
    pub fn new(data: T) -> CpArc<T> {
        // Start the weak pointer count as 1 which is the weak pointer that's
        // held by all the strong pointers (kinda), see std/rc.rs for more info
        let x: Box<_> = box ArcInner {
            strong: atomic::AtomicUsize::new(1),
            cpref:atomic::AtomicUsize::new(0),
            weak: atomic::AtomicUsize::new(1),
            /* total = strong + cpref */
            total: atomic::AtomicUsize::new(1),
            data: data,
            cpdata: None,
        };
        CpArc { ptr: unsafe { Shared::new(Box::into_raw(x)) }, checkpointed : false }
    }

    /// Unwraps the contained value if the `CpArc<T>` has exactly one strong reference.
    ///
    /// Otherwise, an `Err` is returned with the same `CpArc<T>`.
    ///
    /// This will succeed even if there are outstanding weak references.
    ///
    /// # Examples
    ///
    /// ```
    /// use std::sync::CpArc;
    ///
    /// let x = CpArc::new(3);
    /// assert_eq!(CpArc::try_unwrap(x), Ok(3));
    ///
    /// let x = CpArc::new(4);
    /// let _y = x.clone();
    /// assert_eq!(CpArc::try_unwrap(x), Err(CpArc::new(4)));
    /// ```
    #[inline]
    pub fn try_unwrap(this: Self) -> Result<T, Self> {
        // See `drop` for why all these atomics are like this
        if this.inner().strong.compare_exchange(1, 0, Release, Relaxed).is_err() {
            return Err(this);
        }

        atomic::fence(Acquire);

        unsafe {
            let ptr = *this.ptr;
            let elem = ptr::read(&(*ptr).data);

            // Make a weak pointer to clean up the implicit strong-weak reference
            let _weak = Weak { ptr: this.ptr };
            mem::forget(this);

            Ok(elem)
        }
    }
    
    pub fn unwrap_cpdata(&self) -> &T {
        // See `drop` for why all these atomics are like this
        &self.inner().cpdata.as_ref().unwrap()
    }

    pub fn restore(&mut self) -> CpArc<T> {
        
        if !self.checkpointed {
            panic!("CpArc can be restored only when checkpointed");    
        }

        /*TODO what happens when strong and weak references to data exists? */

        /* Reset cpref and */
        self.inner().cpref.store(1, Release);
        self.checkpointed = false;
        CpArc { ptr: self.ptr, checkpointed: false }
    }
}

impl<T: ?Sized> CpArc<T> {
    /// Downgrades the `CpArc<T>` to a `Weak<T>` reference.
    ///
    /// # Examples
    ///
    /// ```
    /// use std::sync::CpArc;
    ///
    /// let five = CpArc::new(5);
    ///
    /// let weak_five = CpArc::downgrade(&five);
    /// ```
    pub fn downgrade(this: &Self) -> Weak<T> {
        // This Relaxed is OK because we're checking the value in the CAS
        // below.
        let mut cur = this.inner().weak.load(Relaxed);

        loop {
            // check if the weak counter is currently "locked"; if so, spin.
            if cur == usize::MAX {
                cur = this.inner().weak.load(Relaxed);
                continue;
            }

            // NOTE: this code currently ignores the possibility of overflow
            // into usize::MAX; in general both Rc and CpArc need to be adjusted
            // to deal with overflow.

            // Unlike with Clone(), we need this to be an Acquire read to
            // synchronize with the write coming from `is_unique`, so that the
            // events prior to that write happen before this read.
            match this.inner().weak.compare_exchange_weak(cur, cur + 1, Acquire, Relaxed) {
                Ok(_) => return Weak { ptr: this.ptr },
                Err(old) => cur = old,
            }
        }
    }

    /// Get the number of weak references to this value.
    #[inline]
    pub fn weak_count(this: &Self) -> usize {
        this.inner().weak.load(SeqCst) - 1
    }

    /// Get the number of strong references to this value.
    #[inline]
    pub fn strong_count(this: &Self) -> usize {
        this.inner().strong.load(SeqCst)
    }

    #[inline]
    pub fn cpref_count(this: &Self) -> usize {
        this.inner().cpref.load(SeqCst)
    }

    #[inline]
    fn inner(&self) -> &ArcInner<T> {
        // This unsafety is ok because while this arc is alive we're guaranteed
        // that the inner pointer is valid. Furthermore, we know that the
        // `ArcInner` structure itself is `Sync` because the inner data is
        // `Sync` as well, so we're ok loaning out an immutable pointer to these
        // contents.
        unsafe { &**self.ptr }
    }

    #[inline]
    fn inner_mut(&self) -> &mut ArcInner<T> {
        // This unsafety is ok because while this arc is alive we're guaranteed
        // that the inner pointer is valid. Furthermore, we know that the
        // `ArcInner` structure itself is `Sync` because the inner data is
        // `Sync` as well, so we're ok loaning out an immutable pointer to these
        // contents.
        unsafe { &mut **self.ptr }
    }

    // Non-inlined part of `drop`.
    #[inline(never)]
    unsafe fn drop_slow(&mut self) {
        let ptr = *self.ptr;

        if self.inner().weak.fetch_sub(1, Release) == 1 {
            atomic::fence(Acquire);
            deallocate(ptr as *mut u8, size_of_val(&*ptr), align_of_val(&*ptr))
        }
    }
}

impl<T: ?Sized> CheckPoint for CpArc<T>
        where T : Sized,
        T: std::fmt::Display {
    fn checkpt(&self) -> Self {
        
        let old_count = self.inner().cpref.fetch_add(1, Relaxed);
        
        if old_count == 0 {
            unsafe {
                let ptr = *self.ptr;
                let elem = ptr::read(&(*ptr).data);                
                self.inner_mut().cpdata = Some(box elem.checkpt());
            }
        }
       
        if old_count > MAX_REFCOUNT {
            unsafe {    
                abort();
            }
        }
        /* This is to prevent racy drop of ArcInner */
        self.inner().total.fetch_add(1, Relaxed);
        
        CpArc { ptr: self.ptr, checkpointed: true }
    }
}

impl<T: ?Sized> Clone for CpArc<T> {
    /// Makes a clone of the `CpArc<T>`.
    ///
    /// This increases the strong reference count.
    ///
    /// # Examples
    ///
    /// ```
    /// use std::sync::CpArc;
    ///
    /// let five = CpArc::new(5);
    ///
    /// five.clone();
    /// ```
    #[inline]
    fn clone(&self) -> CpArc<T> {

        if self.checkpointed {
            panic!("Cannot clone a checkpointed CpArc");
        }
        // Using a relaxed ordering is alright here, as knowledge of the
        // original reference prevents other threads from erroneously deleting
        // the object.
        //
        // As explained in the [Boost documentation][1], Increasing the
        // reference counter can always be done with memory_order_relaxed: New
        // references to an object can only be formed from an existing
        // reference, and passing an existing reference from one thread to
        // another must already provide any required synchronization.
        //
        // [1]: (www.boost.org/doc/libs/1_55_0/doc/html/atomic/usage_examples.html)
        let old_size = self.inner().strong.fetch_add(1, Relaxed);

        // However we need to guard against massive refcounts in case someone
        // is `mem::forget`ing CpArcs. If we don't do this the count can overflow
        // and users will use-after free. We racily saturate to `isize::MAX` on
        // the assumption that there aren't ~2 billion threads incrementing
        // the reference count at once. This branch will never be taken in
        // any realistic program.
        //
        // We abort because such a program is incredibly degenerate, and we
        // don't care to support it.
        if old_size > MAX_REFCOUNT {
            unsafe {
                abort();
            }
        }

        /* This is to prevent racy drop of ArcInner */
        self.inner().total.fetch_add(1, Relaxed);
 
        CpArc { ptr: self.ptr, checkpointed: false }
    }
}

impl<T: ?Sized> Deref for CpArc<T> {
    type Target = T;

    #[inline]
    fn deref(&self) -> &T {
        &self.inner().data
    }
}

impl<T: Clone> CpArc<T> {
    /// Make a mutable reference into the given `CpArc<T>`.
    /// If the `CpArc<T>` has more than one strong reference, or any weak
    /// references, the inner data is cloned.
    ///
    /// This is also referred to as a copy-on-write.
    ///
    /// # Examples
    ///
    /// ```
    /// use std::sync::CpArc;
    ///
    /// let mut data = CpArc::new(5);
    ///
    /// *CpArc::make_mut(&mut data) += 1;         // Won't clone anything
    /// let mut other_data = data.clone();      // Won't clone inner data
    /// *CpArc::make_mut(&mut data) += 1;         // Clones inner data
    /// *CpArc::make_mut(&mut data) += 1;         // Won't clone anything
    /// *CpArc::make_mut(&mut other_data) *= 2;   // Won't clone anything
    ///
    /// // Note: data and other_data now point to different numbers
    /// assert_eq!(*data, 8);
    /// assert_eq!(*other_data, 12);
    ///
    /// ```
    #[inline]
    pub fn make_mut(this: &mut Self) -> &mut T {
        // Note that we hold both a strong reference and a weak reference.
        // Thus, releasing our strong reference only will not, by itself, cause
        // the memory to be deallocated.
        //
        // Use Acquire to ensure that we see any writes to `weak` that happen
        // before release writes (i.e., decrements) to `strong`. Since we hold a
        // weak count, there's no chance the CpArcInner itself could be
        // deallocated.
        if this.inner().strong.compare_exchange(1, 0, Acquire, Relaxed).is_err() {
            // Another strong pointer exists; clone
            *this = CpArc::new((**this).clone());
        } else if this.inner().weak.load(Relaxed) != 1 {
            // Relaxed suffices in the above because this is fundamentally an
            // optimization: we are always racing with weak pointers being
            // dropped. Worst case, we end up allocated a new CpArc unnecessarily.

            // We removed the last strong ref, but there are additional weak
            // refs remaining. We'll move the contents to a new CpArc, and
            // invalidate the other weak refs.

            // Note that it is not possible for the read of `weak` to yield
            // usize::MAX (i.e., locked), since the weak count can only be
            // locked by a thread with a strong reference.

            // Materialize our own implicit weak pointer, so that it can clean
            // up the ArcInner as needed.
            let weak = Weak { ptr: this.ptr };

            // mark the data itself as already deallocated
            unsafe {
                // there is no data race in the implicit write caused by `read`
                // here (due to zeroing) because data is no longer accessed by
                // other threads (due to there being no more strong refs at this
                // point).
                let mut swap = CpArc::new(ptr::read(&(**weak.ptr).data));
                mem::swap(this, &mut swap);
                mem::forget(swap);
            }
        } else {
            // We were the sole reference of either kind; bump back up the
            // strong ref count.
            this.inner().strong.store(1, Release);
        }

        // As with `get_mut()`, the unsafety is ok because our reference was
        // either unique to begin with, or became one upon cloning the contents.
        unsafe {
            let inner = &mut **this.ptr;
            &mut inner.data
        }
    }
}

impl<T: ?Sized> CpArc<T> {
    /// Returns a mutable reference to the contained value if the `CpArc<T>` has
    /// one strong reference and no weak references.
    ///
    /// # Examples
    ///
    /// ```
    /// use std::sync::CpArc;
    ///
    /// let mut x = CpArc::new(3);
    /// *CpArc::get_mut(&mut x).unwrap() = 4;
    /// assert_eq!(*x, 4);
    ///
    /// let _y = x.clone();
    /// assert!(CpArc::get_mut(&mut x).is_none());
    /// ```
    #[inline]
    pub fn get_mut(this: &mut Self) -> Option<&mut T> {
        if this.is_unique() {
            // This unsafety is ok because we're guaranteed that the pointer
            // returned is the *only* pointer that will ever be returned to T. Our
            // reference count is guaranteed to be 1 at this point, and we required
            // the CpArc itself to be `mut`, so we're returning the only possible
            // reference to the inner data.
            unsafe {
                let inner = &mut **this.ptr;
                Some(&mut inner.data)
            }
        } else {
            None
        }
    }

    /// Determine whether this is the unique reference (including weak refs) to
    /// the underlying data.
    ///
    /// Note that this requires locking the weak ref count.
    fn is_unique(&mut self) -> bool {
        // lock the weak pointer count if we appear to be the sole weak pointer
        // holder.
        //
        // The acquire label here ensures a happens-before relationship with any
        // writes to `strong` prior to decrements of the `weak` count (via drop,
        // which uses Release).
        if self.inner().weak.compare_exchange(1, usize::MAX, Acquire, Relaxed).is_ok() {
            // Due to the previous acquire read, this will observe any writes to
            // `strong` that were due to upgrading weak pointers; only strong
            // clones remain, which require that the strong count is > 1 anyway.
            let unique = self.inner().strong.load(Relaxed) == 1;

            // The release write here synchronizes with a read in `downgrade`,
            // effectively preventing the above read of `strong` from happening
            // after the write.
            self.inner().weak.store(1, Release); // release the lock
            unique
        } else {
            false
        }
    }
}

impl<T: ?Sized> Drop for CpArc<T> {
    /// Drops the `CpArc<T>`.
    ///
    /// This will decrement the strong reference count. If the strong reference
    /// count becomes zero and the only other references are `Weak<T>` ones,
    /// `drop`s the inner value.
    ///
    /// # Examples
    ///
    /// ```
    /// use std::sync::CpArc;
    ///
    /// {
    ///     let five = CpArc::new(5);
    ///
    ///     // stuff
    ///
    ///     drop(five); // explicit drop
    /// }
    /// {
    ///     let five = CpArc::new(5);
    ///
    ///     // stuff
    ///
    /// } // implicit drop
    /// ```
    #[unsafe_destructor_blind_to_params]
    #[inline]
    fn drop(&mut self) {
        // Because `fetch_sub` is already atomic, we do not need to synchronize
        // with other threads unless we are going to delete the object. This
        // same logic applies to the below `fetch_sub` to the `weak` count.
        //
        // Drop the data only if both reference counts reaches 1
        //
        // For CpArc, if checkpointed references are active but *no* strong and weak references are
        // active, then we should prevent destroying the complete instance of ArcInner. On the
        // other hand if strong or weak references are active but *no* cprefs then we can just
        // decrement cprefs and set cpdata to None
 
        // This fence is needed to prevent reordering of use of the data and
        // deletion of the data.  Because it is marked `Release`, the decreasing
        // of the reference count synchronizes with this `Acquire` fence. This
        // means that use of the data happens before decreasing the reference
        // count, which happens before this fence, which happens before the
        // deletion of the data.
        //
        // As explained in the [Boost documentation][1],
        //
        // > It is important to enforce any possible access to the object in one
        // > thread (through an existing reference) to *happen before* deleting
        // > the object in a different thread. This is achieved by a "release"
        // > operation after dropping a reference (any access to the object
        // > through this reference must obviously happened before), and an
        // > "acquire" operation before deleting the object.
        //
        // [1]: (www.boost.org/doc/libs/1_55_0/doc/html/atomic/usage_examples.html)
       
        if self.checkpointed {
            if self.inner().cpref.fetch_sub(1, Release) != 1 {
                return;
            }
            
            self.inner_mut().cpdata = None;    
            
        } else {
            if self.inner().strong.fetch_sub(1, Release) != 1 {
                return;
            }

            // Destroy the data at this time, even though we may not free the box
            // allocation itself (there may still be weak pointers lying around).
            unsafe {
                let ptr = *self.ptr;
                ptr::drop_in_place(&mut (*ptr).data);
            }
        }

        if self.inner().total.fetch_sub(1, Release) == 1 {
            atomic::fence(Acquire);
            unsafe {
                self.drop_slow();
            }
        }

    }
}

impl<T> Weak<T> {
    /// Constructs a new `Weak<T>` without an accompanying instance of T.
    ///
    /// This allocates memory for T, but does not initialize it. Calling
    /// Weak<T>::upgrade() on the return value always gives None.
    ///
    /// # Examples
    ///
    /// ```
    /// use std::sync::Weak;
    ///
    /// let empty: Weak<i64> = Weak::new();
    /// ```
    pub fn new() -> Weak<T> {
        unsafe {
            Weak {
                ptr: Shared::new(Box::into_raw(box ArcInner {
                    strong: atomic::AtomicUsize::new(0),
                    cpref: atomic::AtomicUsize::new(0),
                    weak: atomic::AtomicUsize::new(1),
                    total: atomic::AtomicUsize::new(1),
                    data: uninitialized(),
                    cpdata: None,
                })),
            }
        }
    }
}

impl<T: ?Sized> Weak<T> {
    /// Upgrades a weak reference to a strong reference.
    ///
    /// Upgrades the `Weak<T>` reference to an `CpArc<T>`, if possible.
    ///
    /// Returns `None` if there were no strong references and the data was
    /// destroyed.
    ///
    /// # Examples
    ///
    /// ```
    /// use std::sync::CpArc;
    ///
    /// let five = CpArc::new(5);
    ///
    /// let weak_five = CpArc::downgrade(&five);
    ///
    /// let strong_five: Option<CpArc<_>> = weak_five.upgrade();
    /// ```
    pub fn upgrade(&self) -> Option<CpArc<T>> {
        // We use a CAS loop to increment the strong count instead of a
        // fetch_add because once the count hits 0 it must never be above 0.
        let inner = self.inner();

        // Relaxed load because any write of 0 that we can observe
        // leaves the field in a permanently zero state (so a
        // "stale" read of 0 is fine), and any other value is
        // confirmed via the CAS below.
        let mut n = inner.strong.load(Relaxed);

        loop {
            if n == 0 {
                return None;
            }

            // See comments in `CpArc::clone` for why we do this (for `mem::forget`).
            if n > MAX_REFCOUNT {
                unsafe {
                    abort();
                }
            }

            // Relaxed is valid for the same reason it is on CpArc's Clone impl
            match inner.strong.compare_exchange_weak(n, n + 1, Relaxed, Relaxed) {
                Ok(_) => return Some(CpArc { ptr: self.ptr, checkpointed: false }),
                Err(old) => n = old,
            }
        }
    }

    #[inline]
    fn inner(&self) -> &ArcInner<T> {
        // See comments above for why this is "safe"
        unsafe { &**self.ptr }
    }
}

impl<T: ?Sized> Clone for Weak<T> {
    /// Makes a clone of the `Weak<T>`.
    ///
    /// This increases the weak reference count.
    ///
    /// # Examples
    ///
    /// ```
    /// use std::sync::CpArc;
    ///
    /// let weak_five = CpArc::downgrade(&CpArc::new(5));
    ///
    /// weak_five.clone();
    /// ```
    #[inline]
    fn clone(&self) -> Weak<T> {
        // See comments in CpArc::clone() for why this is relaxed.  This can use a
        // fetch_add (ignoring the lock) because the weak count is only locked
        // where are *no other* weak pointers in existence. (So we can't be
        // running this code in that case).
        let old_size = self.inner().weak.fetch_add(1, Relaxed);

        // See comments in CpArc::clone() for why we do this (for mem::forget).
        if old_size > MAX_REFCOUNT {
            unsafe {
                abort();
            }
        }

        return Weak { ptr: self.ptr };
    }
}

impl<T> Default for Weak<T> {
    fn default() -> Weak<T> {
        Weak::new()
    }
}

impl<T: ?Sized> Drop for Weak<T> {
    /// Drops the `Weak<T>`.
    ///
    /// This will decrement the weak reference count.
    ///
    /// # Examples
    ///
    /// ```
    /// use std::sync::CpArc;
    ///
    /// {
    ///     let five = CpArc::new(5);
    ///     let weak_five = CpArc::downgrade(&five);
    ///
    ///     // stuff
    ///
    ///     drop(weak_five); // explicit drop
    /// }
    /// {
    ///     let five = CpArc::new(5);
    ///     let weak_five = CpArc::downgrade(&five);
    ///
    ///     // stuff
    ///
    /// } // implicit drop
    /// ```
    fn drop(&mut self) {
        let ptr = *self.ptr;

        // If we find out that we were the last weak pointer, then its time to
        // deallocate the data entirely. See the discussion in CpArc::drop() about
        // the memory orderings
        //
        // It's not necessary to check for the locked state here, because the
        // weak count can only be locked if there was precisely one weak ref,
        // meaning that drop could only subsequently run ON that remaining weak
        // ref, which can only happen after the lock is released.
        if self.inner().weak.fetch_sub(1, Release) == 1 {
            atomic::fence(Acquire);
            unsafe { deallocate(ptr as *mut u8, size_of_val(&*ptr), align_of_val(&*ptr)) }
        }
    }
}

impl<T: ?Sized + PartialEq> PartialEq for CpArc<T> {
    /// Equality for two `CpArc<T>`s.
    ///
    /// Two `CpArc<T>`s are equal if their inner value are equal.
    ///
    /// # Examples
    ///
    /// ```
    /// use std::sync::CpArc;
    ///
    /// let five = CpArc::new(5);
    ///
    /// five == CpArc::new(5);
    /// ```
    fn eq(&self, other: &CpArc<T>) -> bool {
        *(*self) == *(*other)
    }

    /// Inequality for two `CpArc<T>`s.
    ///
    /// Two `CpArc<T>`s are unequal if their inner value are unequal.
    ///
    /// # Examples
    ///
    /// ```
    /// use std::sync::CpArc;
    ///
    /// let five = CpArc::new(5);
    ///
    /// five != CpArc::new(5);
    /// ```
    fn ne(&self, other: &CpArc<T>) -> bool {
        *(*self) != *(*other)
    }
}
impl<T: ?Sized + PartialOrd> PartialOrd for CpArc<T> {
    /// Partial comparison for two `CpArc<T>`s.
    ///
    /// The two are compared by calling `partial_cmp()` on their inner values.
    ///
    /// # Examples
    ///
    /// ```
    /// use std::sync::CpArc;
    ///
    /// let five = CpArc::new(5);
    ///
    /// five.partial_cmp(&CpArc::new(5));
    /// ```
    fn partial_cmp(&self, other: &CpArc<T>) -> Option<Ordering> {
        (**self).partial_cmp(&**other)
    }

    /// Less-than comparison for two `CpArc<T>`s.
    ///
    /// The two are compared by calling `<` on their inner values.
    ///
    /// # Examples
    ///
    /// ```
    /// use std::sync::CpArc;
    ///
    /// let five = CpArc::new(5);
    ///
    /// five < CpArc::new(5);
    /// ```
    fn lt(&self, other: &CpArc<T>) -> bool {
        *(*self) < *(*other)
    }

    /// 'Less-than or equal to' comparison for two `CpArc<T>`s.
    ///
    /// The two are compared by calling `<=` on their inner values.
    ///
    /// # Examples
    ///
    /// ```
    /// use std::sync::CpArc;
    ///
    /// let five = CpArc::new(5);
    ///
    /// five <= CpArc::new(5);
    /// ```
    fn le(&self, other: &CpArc<T>) -> bool {
        *(*self) <= *(*other)
    }

    /// Greater-than comparison for two `CpArc<T>`s.
    ///
    /// The two are compared by calling `>` on their inner values.
    ///
    /// # Examples
    ///
    /// ```
    /// use std::sync::CpArc;
    ///
    /// let five = CpArc::new(5);
    ///
    /// five > CpArc::new(5);
    /// ```
    fn gt(&self, other: &CpArc<T>) -> bool {
        *(*self) > *(*other)
    }

    /// 'Greater-than or equal to' comparison for two `CpArc<T>`s.
    ///
    /// The two are compared by calling `>=` on their inner values.
    ///
    /// # Examples
    ///
    /// ```
    /// use std::sync::CpArc;
    ///
    /// let five = CpArc::new(5);
    ///
    /// five >= CpArc::new(5);
    /// ```
    fn ge(&self, other: &CpArc<T>) -> bool {
        *(*self) >= *(*other)
    }
}
impl<T: ?Sized + Ord> Ord for CpArc<T> {
    fn cmp(&self, other: &CpArc<T>) -> Ordering {
        (**self).cmp(&**other)
    }
}
impl<T: ?Sized + Eq> Eq for CpArc<T> {}

impl<T: ?Sized + fmt::Display> fmt::Display for CpArc<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Display::fmt(&**self, f)
    }
}

impl<T: ?Sized + fmt::Debug> fmt::Debug for CpArc<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Debug::fmt(&**self, f)
    }
}

impl<T: ?Sized> fmt::Pointer for CpArc<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Pointer::fmt(&*self.ptr, f)
    }
}

impl<T: Default> Default for CpArc<T> {
    fn default() -> CpArc<T> {
        CpArc::new(Default::default())
    }
}

impl<T: ?Sized + Hash> Hash for CpArc<T> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        (**self).hash(state)
    }
}

impl<T> From<T> for CpArc<T> {
    fn from(t: T) -> Self {
        CpArc::new(t)
    }
}
