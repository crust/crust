#![feature(plugin_registrar, rustc_private, box_syntax)]
/*
 * rustc_private is a feature flag, just like any other flag. This feature flag is one that we're
 * never going to stabilize, though: it's so that the Rust compiler can use certain libraries, but
 * not expose them to the outside world.
 * */
extern crate rustc;
extern crate rustc_plugin;

extern crate syntax;
extern crate syntax_ext;

use rustc_plugin::Registry;
use syntax::ext::base::*;
use syntax::parse::token::intern;
use syntax::ptr::P;
use syntax::ast::{Item, MetaItem};
use syntax::codemap::Span;

pub mod cp;

#[plugin_registrar]
pub fn plugin_registrar(reg: &mut Registry) {
    reg.register_syntax_extension(intern("derive_CheckPoint"),
    MultiDecorator(box cp::expand_cp));
}

