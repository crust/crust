\documentclass[12pt]{article}

\usepackage[T1]{fontenc}
\usepackage{times}
\usepackage{cite}
\usepackage{url}
\usepackage{graphicx}

\begin{document}

\sloppy

\title{Implementing Checkpointing in Rust}
\author{Leonid Ryzhyk}

\maketitle

\section{Checkpointing}

We discuss how Rust can simplify the implementation of 
\emph{in-memory checkpointing}, i.e., the ability to make a copy 
of the object hierarchy at runtime and later restore the object 
from the checkpoint.  A good checkpointing mechanism must be (a) 
easy to adopt (no or minimal changes to existing code), (b) fast 
(minimal runtime overhead), (c) safe (should not create dangling 
pointers or violate any other type and memory safety constraints), 
(d) idempotent (the precise definition is actually tricky, but 
intuitively we want the checkpoint+restore checkpoint sequence to 
behave as a no-op).

Consider several example data structures shown in 
Figure~\ref{f:examples} and the difficulties that arise in 
checkpointing these data structures. Arrows represent pointers; 
the dashed rectangle shows boundaries of the checkpointed object.  
Figure~\ref{f:examples}a shows the simplest case, where the 
checkpointed data structure does not contain any aliases or remote 
references.  Checkpointing such a data structure is easy in any 
language, including C, as long as the size of each object is known 
at compile time.

\begin{figure}
    \center
    \includegraphics[width=0.8\linewidth]{examples.pdf}
    \caption{}\label{f:examples}
\end{figure}

In Figure~\ref{f:examples}b, the data structure contains an 
internal alias.  This requires special care in languages like C++ 
and Java to keep track of the set of addresses that have already 
been traversed and check every traversed reference for membership 
in this set.  Otherwise, we risk creating multiple copies of the 
aliased object.  This introduces extra runtime overhead.

In Figure~\ref{f:examples}c, there exists an external reference to 
the checkpointed data structure.  There is a risk that after 
restoring the checkpointed object to a new address in memory, the 
external reference will either become dangling or a redundant copy 
of the aliased object will be created.  There is no way to detect 
this in either C++ or Java; one must rely on a coding discipline 
that ensures that these situations don't occur.  

Figure~\ref{f:examples}d shows a futher twist of the previous 
problem: an external reference is created after the data structure 
was checkpointed.  The difference here is that there does not 
exist a way to restore the checkpoint that maintains a consistent 
state of the object hierarchy.

Figure~\ref{f:examples}e shows the case where we would like to 
checkpoint part of the object hierarchy that contains an external 
reference to an object that belongs to a different ``checkpoint 
domain''.  This can lead to creating a dangling reference during 
recovery if the external object is destroyed.  Similar to the 
previous case, this problem does not have a good general-purpose 
solution.

\subsection{Checkpointing in Rust}

The proposed mechanism relies on the fact that aliasing in Rust is 
always explicit.  In both Java and C++, any reference can 
potentially be an alias, which makes safe checkpoint hard.  In 
contrast, in Rust, unless the reference is wrapped in 
\texttt{Arc}, \texttt{Rc} or another wrapper of this sort, it is 
guaranteed to not be aliased.  

The discussion points below are half-baked ideas, each of which 
requires a separate discussion.

\paragraph{Discussion point 1} Figure~\ref{f:examples}a represents 
basic, ``pure'' Rust.  Such data structures can be checkpointed 
fully automatically and efficiently, without requiring the 
developer to use any special libraries of checkpointable data 
types and without compromising safety.  

\paragraph{Discussion point 2} Situations (b) and (c) can also be 
handled in a fully safe (including tread-safe) way by implementing 
special checkpoint-friendly versions of \texttt{Arc}, \texttt{Rc}, 
and other relevant wrappers.  

\paragraph{Discussion point 3} Situations (d) and (e) represent 
cases where some higher-level, application- or framework- specific 
coordination is required to restore global consistency after 
recovery.  We should aim to implement low-level mechanisms that 
enable such coordination in the checkpointing library.  
Fortunately, these mechanisms will again be localized to the set 
of data types that implement aliasing (\texttt{Arc} and friends).  

Note that all off the above mechanisms do not require 
modifications to existing Rust programs, other than switching to 
checkpoint-aware implementation of (\texttt{Arc} and friends).

\subsection{Lazy checkpointing in Rust}

\paragraph{Discussion point 4} Lazy checkpointing means that the 
actual copy operation is delayed until the checkpointed object is 
being modified.  We can achieve this using a modified version of 
the \texttt{Cell} type.  Specifically, we wrap every object that 
will be checkpointed lazily on this modified \texttt{Cell}.  The 
only way to modify the content of cell is by first ontaining a 
mutable reference to its internal object via the 
\texttt{get\_mut()} method.  Our modified implementation will 
delay the copying until the first call to \texttt{get\_mut()}.  
This design ensures that only write accesses to the object trigger 
the copyint.

This mechanism requires minor modification to the existing 
implementation and introduces modest runtime overhead due to an 
extra level of indirection via \texttt{Cell}.  However, the 
required modifications are trivial.  Besides, the decision to wrap 
part of the object in a \texttt{Cell} may have a slight effect on
performance, but not on safety.

\end{document}
