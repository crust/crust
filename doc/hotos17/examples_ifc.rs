use std::vec::Vec;

struct Buffer {
  data: Option<Vec<u8>>
}

impl Buffer {
   fn new() -> Buffer {Buffer{data: None}}
   fn append(&mut self, mut v: Vec<u8>) {
     match self.data {
       None            => self.data = Some(v),
       Some(ref mut d) => d.append(&mut v)
     }
   }
}

fn main() {
  let mut buf = Buffer::new();
  let nonsecret = vec![1,2,3];
  let secret = vec![4,5,6];

  //buf.append(nonsecret);
  buf.append(secret);
  println!("{:?}", buf.data);
  println!("{:?}", nonsecret);
}
