pub mod label {
    use std::cmp::max;
    pub type Label = u32;
    pub fn combine_labels(l1: Label, l2: Label) -> Label {
        max(l1,l2)
    }
}

pub use self::verify::*;
#[macro_use]
mod verify;
pub mod sec_vec;
